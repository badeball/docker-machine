package azureutil

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/arm/compute"
	"github.com/Azure/go-autorest/autorest/to"
)

/* Utilities */

// randomAzureStorageAccountName generates a valid storage account name prefixed
// with a predefined string. Availability of the name is not checked. Uses maximum
// length to maximise randomness.
func randomAzureStorageAccountName() string {
	const (
		maxLen = 24
		chars  = "0123456789abcdefghijklmnopqrstuvwxyz"
	)
	return storageAccountPrefix + randomString(maxLen-len(storageAccountPrefix), chars)
}

// randomString generates a random string of given length using specified alphabet.
func randomString(n int, alphabet string) string {
	r := timeSeed()
	b := make([]byte, n)
	for i := range b {
		b[i] = alphabet[r.Intn(len(alphabet))]
	}
	return string(b)
}

// parseImageName parses a publisher:offer:sku:version into those parts.
func parseImageName(image string) (*compute.ImageReference, error) {
	if strings.Contains(strings.ToLower(image), "/images/") {
		// image represents an ARM resource identifer for a custom image
		return &compute.ImageReference{
			ID: to.StringPtr(image),
		}, nil
	}
	l := strings.Split(image, ":")
	if len(l) != 4 {
		return nil, fmt.Errorf("image name %q not a valid format", image)
	}
	return &compute.ImageReference{
		Publisher: to.StringPtr(l[0]),
		Offer:     to.StringPtr(l[1]),
		Sku:       to.StringPtr(l[2]),
		Version:   to.StringPtr(l[3]),
	}, nil
}

func timeSeed() *rand.Rand { return rand.New(rand.NewSource(time.Now().UTC().UnixNano())) }
